import './App.css';
import StudentComponent from './StudentComponent/StudentComponent';
import StudentList from './StudentComponent/StudentList';

function App() {
  return (
    <div>
      <StudentComponent/>
    </div>
  );
}

export default App;
