import { combineReducers } from "redux";
import { managermentStudentReducer } from "./managermentStudentReducer";

export const rootReducer  = combineReducers({managermentStudentReducer: managermentStudentReducer})