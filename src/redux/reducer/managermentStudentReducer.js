import { ADD_STUDENT, DELETE_STUDENT, UPDATE_LIST } from "../constant/studentConstant";

const initialState  = {
    listStudent: [
        {
            id: "025",
            name:"Nguyễn Văn A",
            phone:"098563211",
            email:"nguyenvana@gmail.com"
        },
    ]
}

export const managermentStudentReducer = (state = initialState, {type, payload}) => { 
    switch (type) {
        case ADD_STUDENT: {
            let newListStudent = [...state.listStudent, payload];
            state.listStudent = newListStudent;
            return{...state}
        
        }
            break;
        case DELETE_STUDENT: {
            let newListStudent = [...state.listStudent];
            let index = newListStudent.findIndex((sv) => { 
                return sv.id === payload.id;
             })
             if (index > -1) {
                newListStudent.splice(index, 1)
             }
             state.listStudent = newListStudent;
             return {...state}
        }
        break;
        case UPDATE_LIST: {
            let newListStudent = [...state.listStudent];
            let index = newListStudent.findIndex((sv) => { 
                return sv.id === payload.id;
             })
               
             
             if(index > -1) {
                newListStudent[index] = payload;
             }
             state.listStudent = newListStudent;
             return {...state}
        }
    
        default:
            return {...state};
            break;
    }
 }