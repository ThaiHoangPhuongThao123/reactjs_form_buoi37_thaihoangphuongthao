import React, { Component } from "react";
import StudentList from "./StudentList";
import { connect } from "react-redux";

class ManagermentStudent extends Component {
  state = {
    values: {
      id: "",
      name: "",
      phone: "",
      email: "",
    },
    errors: {
      id: "",
      name: "",
      phone: "",
      email: "",
    },
    validated: "",
    editingStudent: null,
  };
  handleFormValid = () => {
    let isValid = true;
    Object.values(this.state.values).forEach((value) => {
      if (!value) {
        isValid = false;
      }
    });
    Object.values(this.state.errors).forEach((value) => {
      if (value) {
        isValid = false;
      }
    });
    return isValid;
  };
  handleOnchange = (event) => {
    let { name, value, pattern } = event.target;
    let newValue = { ...this.state.values, [name]: value };
    let newError = { ...this.state.errors };
    // kiểm tra rông
    if (!value.trim()) {
      newError[name] = name + " không được bỏ trống";
    } else {
      if (pattern) {
        let regex = new RegExp(pattern);
        let valid = regex.test(value);
        if (!valid) {
          newError[name] = name + " không đúng định dạng";
        } else {
          newError[name] = "";
        }
      } else {
        newError[name] = "";
      }
    }
    this.setState({ values: newValue, errors: newError });
  };
  handleBlur = (event) => {
    let { name, value, pattern } = event.target;
    let newValue = { ...this.state.values, [name]: value };
    let newError = { ...this.state.errors };
    // kiểm tra rông
    if (!value.trim()) {
      newError[name] = name + " không được bỏ trống";
    } else {
      if (pattern) {
        let regex = new RegExp(pattern);
        let valid = regex.test(value);
        if (!valid) {
          newError[name] = name + " không đúng định dạng";
        } else {
          newError[name] = "";
        }
      } else {
        newError[name] = "";
      }
    }
    this.setState({ values: newValue, errors: newError });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    console.log("handleFormValid(): ", this.handleFormValid());

    if (this.handleFormValid()) {
      let newListStudent = [...this.props.listStudent];
      let newValidated = "";
      let index = newListStudent.findIndex((sv) => {
        return sv.id === this.state.values.id;
      });
      if (index === -1) {
        this.props.handleAdd(this.state.values);
        newValidated = "";
        this.setState({
        validated: newValidated,
        values: {
          id: "",
          name: "",
          phone: "",
          email: "",
        },
      });
      } else {
        newValidated = "ID đã tồn tại";
        this.setState({
        validated: newValidated,
      });
      }
    
    }
  };
  handleEidt = (student) => {
    let newListStudent = [...this.props.listStudent];
    let index = newListStudent.findIndex((sv) => {
      return sv.id === student.id;
    });
    if (index > -1) {
      this.setState({
        values: newListStudent[index],
        editingStudent: newListStudent[index],
      });
    }
  };
  //  handleUpdate= (student) => {
  //   let newListStudent = [...this.props.listStudent];
  //   let newValue = {...this.state.values}
  //   let index = newListStudent.findIndex((sv) => {
  //     return sv.id === student.id
  //    })
  //    if (index > -1) {
  //     newListStudent[index] = newValue;
  //    }
  //    return newListStudent;
  //   }
  render() {
    let { values, errors, validated, editingStudent } = this.state;
    let { id, name, phone, email } = values;
    let { handleUpdate } = this.props;
    return (
      <div className="container">
        <div className="card text-left">
          <div className="card-header bg-info text-white">
            <h3> CREATE INFORMATION STUDENT</h3>
          </div>
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="col-6">
                  <label htmlFor="">ID</label>
                  <input
                    type="text"
                    name="id"
                    id="id"
                    placeholder="Enter Id"
                    className="form-control"
                    value={id}
                    pattern="^[a-zA-Z0-9]{1,6}$"
                    onChange={this.handleOnchange}
                    onBlur={this.handleBlur}
                    disabled={this.state.editingStudent}
                  />
                  {errors.id && (
                    <span className="text-danger font-weight-bold">
                      {errors.id}
                    </span>
                  )}
                </div>
                <div className="col-6">
                  <label htmlFor="">NAME</label>
                  <input
                    type="text"
                    name="name"
                    id="name"
                    placeholder="Enter Name"
                    className="form-control"
                    value={name}
                    onChange={this.handleOnchange}
                    onBlur={this.handleBlur}
                  />
                  {errors.name && (
                    <span className="text-danger font-weight-bold">
                      {errors.name}
                    </span>
                  )}
                </div>
                <div className="col-6">
                  <label htmlFor="">PHONE</label>
                  <input
                    type="text"
                    name="phone"
                    id="phone"
                    placeholder="Enter Phone"
                    className="form-control"
                    value={phone}
                    pattern="^[0-9]*$"
                    onChange={this.handleOnchange}
                    onBlur={this.handleBlur}
                  />
                  {errors.phone && (
                    <span className="text-danger font-weight-bold">
                      {errors.phone}
                    </span>
                  )}
                </div>
                <div className="col-6">
                  <label htmlFor="">EMAIL</label>
                  <input
                    type="text"
                    name="email"
                    id="email"
                    placeholder="Enter Email"
                    className="form-control"
                    value={email}
                    pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"
                    onChange={this.handleOnchange}
                    onBlur={this.handleBlur}
                  />
                  {errors.email && (
                    <span className="text-danger font-weight-bold">
                      {errors.email}
                    </span>
                  )}
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-2">
                  {editingStudent ? (
                    <button
                      className="btn btn-success"
                      type="button"
                      onClick={() => {
                        handleUpdate(values);
                        this.setState({
                          values: {
                            id: "",
                            name: "",
                            phone: "",
                            email: "",
                          },
                          editingStudent: null,
                        });
                      }}
                    >
                      UPDATE INFOR
                    </button>
                  ) : (
                    <button className="btn btn-primary">ADD STUDENT</button>
                  )}
                </div>
              </div>
              <div className="row mt-3">
                <span
                  className="font-weight-bold text-danger "
                  style={{ fontSize: "22px" }}
                >
                  {validated}
                </span>
              </div>
            </form>
          </div>
        </div>
        <StudentList handleEidt={this.handleEidt} />
      </div>
    );
  }
}

const mapDispathToProps = (dispath) => {
  return {
    handleAdd: (sv) => {
      let action = {
        type: "ADD_STUDENT",
        payload: sv,
      };
      return dispath(action);
    },
    handleUpdate: (sv) => {
      let action = {
        type: "UPDATE_LIST",
        payload: sv,
      };
      return dispath(action);
    },
  };
};

const mapStateToProps = (state) => {
  return {
    listStudent: state.managermentStudentReducer.listStudent,
  };
};

export default connect(mapStateToProps, mapDispathToProps)(ManagermentStudent);
