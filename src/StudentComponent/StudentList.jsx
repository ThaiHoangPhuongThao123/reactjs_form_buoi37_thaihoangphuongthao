import React, { Component } from "react";
import { connect } from "react-redux";

class StudentList extends Component {
  render() {
    let { listStudent, handleDelete, handleEidt } = this.props;
    return (
      <div className="mt-5">
        <div className="card text-left">
          <div className="card-header bg-info text-white">
            <h3> LIST STUDENT</h3>
          </div>
          <div className="card-body">
            <table className="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>NAME</th>
                  <th>PHONE</th>
                  <th>EMAIL</th>
                  <th>ACTIONS</th>
                </tr>
              </thead>
              <tbody>
                {listStudent.map((sv) => {
                  let { id, name, phone, email } = sv;
                  return (
                    <tr key={id}>
                      <td itemScope="row">{id}</td>
                      <td>{name}</td>
                      <td>{phone}</td>
                      <td>{email}</td>
                      <td>
                        <button className="btn btn-danger mr-2" onClick={() => { handleDelete(sv) }}>
                          <i className="fa fa-trash"></i> Delete
                        </button>
                        <button className="btn btn-warning" onClick={() => { handleEidt(sv) }}>
                          <i className="fa fa-edit"></i> Eidt
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    listStudent: state.managermentStudentReducer.listStudent,
  };
};

const mapDispatchToProps = (dispath) => { 
  return {
    handleDelete: (sv) => { 
      let action = {
        type:"DELETE_STUDENT",
        payload: sv,
      }
      return dispath(action)
     }
  }
 }
export default connect(mapStateToProps, mapDispatchToProps)(StudentList);
